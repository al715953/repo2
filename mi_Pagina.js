function guardarDatosUsuario ()
{
    var nombre = document.getElementById("txtNombre").value;
    var email = document.getElementById("txtEmail").value;
    var dni = document.getElementById("txtDNI").value;

    localStorage.setItem("nombre",nombre);
    sessionStorage.setItem("nombre",nombre);
    sessionStorage.setItem("email",email);
    localStorage.setItem("email",email);
    localStorage.setItem("dni",dni);
    sessionStorage.setItem("dni",dni);

    var usuario ={"nombre":nombre,"email":email,"dni":dni};
    localStorage.setItem("usuariosST",JSON.stringify(usuario));
    sessionStorage.setItem("usuariosST",JSON.stringify(usuario));
}

function recuperarDatosUsuario ()
{
    var local = document.getElementById("txtNombre").value;
    var session = sessionStorage.getItem("nombre");
    var usuarioRecLoc = localStorage.getItem("usuariosST");
    var usuarioRecSes = sessionStorage.getItem("usuariosST");
    alert(JSON.parse(usuarioRecLoc).email);
//    alert ("local: "+local + "Session: "+ session);
}
